-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2017 at 04:15 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `sender_id` text,
  `receiver_id` text,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `is_read` enum('0','1') NOT NULL DEFAULT '0',
  `is_draft` enum('0','1') NOT NULL DEFAULT '0',
  `is_delete_sender` enum('0','1') NOT NULL DEFAULT '0',
  `is_delete_receiver` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `sender_id`, `receiver_id`, `subject`, `body`, `is_read`, `is_draft`, `is_delete_sender`, `is_delete_receiver`, `created_at`, `updated_at`) VALUES
(13, '70', '78', 'testing counter', 'qwerty123                    ', '1', '0', '0', '0', '2017-07-31 06:34:03', '2017-07-31 10:04:19'),
(14, '78', '70', 'test', 'check1212                    ', '1', '0', '0', '1', '2017-07-31 06:34:49', '2017-07-31 13:04:55'),
(15, '78', '70', 'newtest', 'xzczcxczxczxzczxcczxcxzczxczxzxcczxzcxzcx                    ', '1', '0', '0', '1', '2017-07-31 06:36:04', '2017-07-31 13:04:55'),
(16, '78', '70', 'testttt', '123123123                    ', '1', '0', '0', '1', '2017-07-31 06:39:49', '2017-07-31 13:06:46'),
(17, '78', '70', 'test2', ' 22222222222                   ', '1', '0', '0', '1', '2017-07-31 06:40:12', '2017-07-31 13:53:12'),
(18, '78', '70', 'reerere', '   rererere                 ', '1', '0', '0', '1', '2017-07-31 09:56:41', '2017-07-31 13:27:16'),
(19, '78', '70', 'rrrrrrrrr', ' rrrrrrrrrrrrrr                   ', '1', '0', '0', '1', '2017-07-31 10:00:29', '2017-07-31 13:33:09'),
(20, '70', '78', 'ttt', 'ssssssssssssssssssssss                    ', '1', '0', '0', '0', '2017-07-31 10:01:12', '2017-07-31 13:56:08'),
(21, '70', '78', 'test3', '33333333                    ', '1', '0', '0', '0', '2017-07-31 10:04:09', '2017-07-31 13:57:33'),
(22, '70', '78', 'test4', '44444444444444                    ', '1', '0', '0', '1', '2017-07-31 10:04:29', '2017-07-31 13:58:19'),
(23, '70', '78', 'test5', '55555555555555555555555555555               ', '1', '0', '0', '0', '2017-07-31 10:05:02', '2017-07-31 13:56:30'),
(24, '78', '70', 'dfthdfghd', '    dsxgfdfgdg                ', '1', '0', '0', '1', '2017-07-31 10:22:58', '2017-07-31 13:56:34'),
(25, '78', '70', 'testing1', '111111111111111111                    ', '1', '0', '0', '1', '2017-07-31 10:23:45', '2017-07-31 13:55:42'),
(26, '78', '70', 'testing delete all', 'aaaaaaaaaaa                    ', '1', '0', '0', '1', '2017-07-31 10:24:11', '2017-07-31 13:55:42'),
(27, '70', '78', 'testing code', 'lorem totem...                    ', '1', '0', '0', '0', '2017-07-31 10:27:11', '2017-07-31 13:57:29'),
(28, '70', '78', 'amit', 'aaaaaaaaaaaaaaaaaaaaaaaaa                    ', '0', '0', '0', '0', '2017-07-31 10:28:40', '2017-07-31 13:58:40'),
(29, '70', '78', 'kkkkkkkkkkk', 'kkkkkkkkkkkkkkkkkkkk                    ', '0', '0', '0', '0', '2017-07-31 10:28:58', '2017-07-31 13:58:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `userEmail` varchar(60) NOT NULL,
  `userPass` varchar(255) NOT NULL,
  `rpass` varchar(20) NOT NULL,
  `image` longtext NOT NULL,
  `bday` date NOT NULL,
  `quali` varchar(50) NOT NULL,
  `state` varchar(10) NOT NULL,
  `Gender` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userName`, `userEmail`, `userPass`, `rpass`, `image`, `bday`, `quali`, `state`, `Gender`) VALUES
(58, 'bikaner', 'bikaner@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500458932.jpg', '2017-07-17', 'English', 'USA', '0'),
(59, 'india', 'india@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500459239.jpg', '2017-08-15', 'English', 'India', '0'),
(60, 'jjn', 'jjn@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500459573.jpg', '2017-07-06', 'English', 'India', '0'),
(61, 'pilani', 'pilani@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1226682954.jpg', '2017-07-04', 'Urdu', 'India', '0'),
(62, 'kolkata1', 'kolkata@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', 'kali.png', '2017-07-16', 'English', 'USA', '0'),
(63, 'joker', 'joker@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1501217519Joker.jpg', '2017-07-04', 'English', 'USA', '0'),
(64, 'neo', 'neo@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500464024.jpg', '2017-07-06', 'English', 'India', '0'),
(65, 'Arya Stark', 'aryastark@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500464336.jpg', '2017-07-17', 'English', 'USA', '0'),
(66, 'Jon Snow', 'jonsnow@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500464966.jpg', '2017-07-15', 'English', 'USA', '0'),
(67, 'ned', 'nedstark@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500465305.jpg', '2017-06-30', 'Hindi', 'India', '0'),
(68, 'GOT', 'got@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '1500464024.jpg', '2017-07-14', 'Urdu', 'USA', '0'),
(69, 'linux', 'linux@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '2017-02-16.jpg', '2017-02-16', 'English', 'USA', '0'),
(70, 'rootuser', 'root@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1501215843Joker.jpg', '2017-07-08', 'English', 'India', '0'),
(71, 'tester', 'testing@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500525187.jpg', '2017-07-06', 'English', 'USA', '0'),
(72, 'batman', 'test@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500525281.jpg', '2017-07-08', 'English', 'India', '0'),
(73, 'supermandfhdfghdfg', 'superman@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500464024.jpg', '2017-07-05', 'English', 'USA', '0'),
(74, 'hulk', 'hulk@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500528359.jpg', '2017-01-05', 'English', 'USA', '0'),
(75, 'Faizal', 'faizal@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500531228.jpg', '2017-07-07', 'Hindi', 'USA', 'M'),
(76, 'the matrix', 'matrix@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500612035mobile.jpg', '2017-07-07', 'English', 'USA', 'M'),
(77, 'leetuser', 'leet@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1500550529mobile.jpg', '2017-07-01', 'English', 'USA', 'F'),
(78, 'king', 'king@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1501132432amit.jpg', '2017-07-07', 'English,Urdu', 'USA', 'M'),
(79, 'Vaibhav', 'vaibhav.bhaiya@ranosys.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', '5e884898da28047151d0', '1500612629.png', '2017-07-19', 'English,Hindi,', 'Others', 'M'),
(80, 'Noobie', 'noobie@mailinator.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '8d969eef6ecad3c29a3a', '1501135943.jpg', '0000-00-00', 'English,Hindi,', 'Russia', 'M');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
